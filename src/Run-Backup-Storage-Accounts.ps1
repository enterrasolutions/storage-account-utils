$destCtx = Get-StorageAccountContext -StorageAccountName $destStorageAccountName
    -StorageAccountResourceGroup $destResourceGroup

  foreach($srcStorageAccount in $srcStorageAccounts)
  {
    $srcStorageAccountName = $srcStorageAccount.storageAccountName
    $srcResourceGroup = $srcStorageAccount.resourceGroup
    $srcCtx = Get-StorageAccountContext -StorageAccountName $srcStorageAccountName
        -StorageAccountResourceGroup $srcResourceGroup
    Backup-StorageAccount -SrcCtx $srcCtx -DestCtx $destCtx
  }